﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform m_Target;

    [SerializeField] private float m_SmoothSpeed = 15;
    [SerializeField] private Vector3 m_LocalOffset;

    private void Update()
    {
        float zOffset = Input.GetAxis("Mouse ScrollWheel");
        m_LocalOffset = new Vector3(m_LocalOffset.x, m_LocalOffset.y, Mathf.Clamp(m_LocalOffset.z + zOffset, -5f, -1.5f));
    }

    private void LateUpdate()
    {
        Vector3 desiredPosition = m_Target.localPosition + m_LocalOffset;
        //Vector3 smoothedPosition = Vector3.Lerp(transform.localPosition, desiredPosition, m_SmoothSpeed * Time.deltaTime);

        transform.localPosition = desiredPosition;
        transform.LookAt(m_Target);
    }
}