﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour
{
    public static DialogManager Instance { get; private set; }

    [SerializeField] private Canvas m_DialogCanvas;
    [SerializeField] private Image m_SpeakerImage;
    [SerializeField] private TextMeshProUGUI m_DialogText;
    [SerializeField] private float m_DialogLineDuration = 1.5f;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_DialogCanvas.gameObject.SetActive(false);
    }

    public void SayAllLines(List<string> lines, Sprite speakerHeadSprite)
    {
        if (m_DialogCanvas.gameObject.activeSelf == true)
            return;
        
        m_DialogCanvas.gameObject.SetActive(true);
        m_SpeakerImage.sprite = speakerHeadSprite;

        StartCoroutine(UnfoldDialogLines(lines));
    }

    public void SayOneRandomLine(List<string> lines, Sprite speakerHeadSprite)
    {
        if (m_DialogCanvas.gameObject.activeSelf == true)
            return;

        m_DialogCanvas.gameObject.SetActive(true);
        m_SpeakerImage.sprite = speakerHeadSprite;

        List<string> randomLine = new List<string>() { lines[Random.Range(0, lines.Count)] };
        StartCoroutine(UnfoldDialogLines(randomLine));
    }

    private IEnumerator UnfoldDialogLines(List<string> lines)
    {
        for(int i = 0; i < lines.Count; i++)
        {
            DisplayDialogLine(lines[i]);

            yield return new WaitForSeconds(m_DialogLineDuration);
        }

        m_DialogCanvas.gameObject.SetActive(false);
    }

    private void DisplayDialogLine(string line)
    {
        m_DialogText.text = line;
    }
}