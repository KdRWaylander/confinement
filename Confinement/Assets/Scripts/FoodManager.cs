﻿using System;
using System.Collections;
using UnityEngine;

public class FoodManager : MonoBehaviour
{
    public static FoodManager Instance { get; private set; }

    public int FoodGoalToReach { get { return m_FoodGoalToReach; } }
    public int CurrentFoodAmount { get { return m_CurrentFoodAmount; } }
    public bool FoodGoalReached => CurrentFoodAmount >= FoodGoalToReach;

    [SerializeField] private Canvas m_FoodCanvas;
    [SerializeField] private int m_FoodGoalToReach;
    [SerializeField] private float m_FoodSpawnProbability = 0.05f;
    [SerializeField] private Transform m_FoodRoot;
    [SerializeField] private GameObject[] m_Foods;
    [SerializeField] private Transform[] m_SpawnableTiles;

    private int m_CurrentFoodAmount;

    
    public event Action NewFoodGoalReached;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        GameManager.Instance.NewPlayModeActivated += StartFoodProcess;
        Player.Instance.NewFoodPickedUp += AddFood;
    }

    private void Start()
    {
        m_FoodCanvas.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        GameManager.Instance.NewPlayModeActivated -= StartFoodProcess;
        Player.Instance.NewFoodPickedUp -= AddFood;
    }

    private IEnumerator SpawnFood()
    {
        foreach(Transform t in m_SpawnableTiles)
        {
            float r = UnityEngine.Random.value;
            if (r <= m_FoodSpawnProbability)
            {
                Vector3 pos = new Vector3(UnityEngine.Random.Range(t.position.x - 5f, t.position.x), 0f, UnityEngine.Random.Range(t.position.z - 5f, t.position.z));
                Quaternion rot = Quaternion.Euler(new Vector3(0, UnityEngine.Random.Range(0, 359), 0));
                Instantiate(m_Foods[UnityEngine.Random.Range(0, m_Foods.Length)], pos, rot, m_FoodRoot);

                yield return null;
            }
        }
    }

    private void StartFoodProcess()
    {
        m_FoodCanvas.gameObject.SetActive(true);
        m_CurrentFoodAmount = 0;
        StartCoroutine(SpawnFood());
    }

    public void AddFood(int amount)
    {
        m_CurrentFoodAmount += amount;

        if (CurrentFoodAmount >= FoodGoalToReach)
            HandleNewFoodGoalReached();
    }

    private void HandleNewFoodGoalReached()
    {
        if (NewFoodGoalReached != null)
            NewFoodGoalReached();
    }
}