﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IntroController : MonoBehaviour
{
    public Sprite Portrait { get; private set; }

    [SerializeField] private GameObject m_Josh;
    [SerializeField] private Sprite m_JoshPortrait;
    [SerializeField] private GameObject m_Allie;
    [SerializeField] private Sprite m_AlliePortrait;
    [SerializeField] private GameObject m_Joe;
    [SerializeField] private Sprite m_JoePortrait;

    [SerializeField] private Vector3 m_Destination;
    [SerializeField] private List<string> m_IntroLines;

    [SerializeField] private NavMeshAgent m_NavMeshAgent;
    [SerializeField] private Animator m_Animator;

    public event Action NewArrivedAtPosition;

    private void Awake()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        LoadCharacter();

        m_NavMeshAgent.SetDestination(m_Destination);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, m_Destination) <= .5f && m_NavMeshAgent.isStopped == false)
        {
            m_NavMeshAgent.ResetPath();
            m_NavMeshAgent.isStopped = true;
            HandleNewArrivedAtPosition();
        }

        m_Animator.SetFloat("Velocity", m_NavMeshAgent.velocity.magnitude);
    }

    private void HandleNewArrivedAtPosition()
    {
        if (NewArrivedAtPosition != null)
            NewArrivedAtPosition();
    }

    private void LoadCharacter()
    {
        if (CharacterManager.Instance.Character == Character.Allie)
        {
            m_Allie.SetActive(true);
            m_Josh.SetActive(false);
            m_Joe.SetActive(false);

            Portrait = m_AlliePortrait;
        }
        else if (CharacterManager.Instance.Character == Character.Josh)
        {
            m_Allie.SetActive(false);
            m_Josh.SetActive(true);
            m_Joe.SetActive(false);

            Portrait = m_JoshPortrait;
        }
        else
        {
            m_Allie.SetActive(false);
            m_Josh.SetActive(false);
            m_Joe.SetActive(true);

            Portrait = m_JoePortrait;
        }
    }
}