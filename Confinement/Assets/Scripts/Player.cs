﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    public string Name { get; private set; }
    public Sprite Portrait { get; private set; }
    public Regime Regime { get; private set; }
    
    [SerializeField] private GameObject m_Josh;
    [SerializeField] private Sprite m_JoshPortrait;
    [SerializeField] private GameObject m_Allie;
    [SerializeField] private Sprite m_AlliePortrait;
    [SerializeField] private GameObject m_Joe;
    [SerializeField] private Sprite m_JoePortrait;

    [SerializeField] private GameObject m_Character;
    [SerializeField] private Camera m_Camera;

    public event Action<int> NewFoodPickedUp;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        GameManager.Instance.NewPlayModeActivated += LoadCharacter;
    }

    private void Start()
    {
        if (GameManager.Instance.GameState == GameState.Victory)
        {
            LoadCharacter();
            return;
        }

        m_Character.SetActive(false);
        m_Camera.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        GameManager.Instance.NewPlayModeActivated -= LoadCharacter;
    }

    public void AddFood(Food food, bool isCompliant)
    {
        if(isCompliant)
        {
            HandleNewFoodPickedUp(food.Amount);
            DialogManager.Instance.SayOneRandomLine(food.CanEatLines, Portrait);
        }
        else
        {
            DialogManager.Instance.SayOneRandomLine(food.CannotEatLines, Portrait);
        }
    }

    public void HandleNewFoodPickedUp(int amount)
    {
        if (NewFoodPickedUp != null)
            NewFoodPickedUp(amount);
    }

    private void LoadCharacter()
    {
        m_Character.SetActive(true);
        m_Camera.gameObject.SetActive(true);

        if(CharacterManager.Instance.Character == Character.Allie)
        {
            m_Allie.SetActive(true);
            m_Josh.SetActive(false);
            m_Joe.SetActive(false);

            Portrait = m_AlliePortrait;
            Regime = Regime.Omnivore;
        }
        else if (CharacterManager.Instance.Character == Character.Josh)
        {
            m_Allie.SetActive(false);
            m_Josh.SetActive(true);
            m_Joe.SetActive(false);

            Portrait = m_JoshPortrait;
            Regime = Regime.Vegan;
        }
        else
        {
            m_Allie.SetActive(false);
            m_Josh.SetActive(false);
            m_Joe.SetActive(true);

            Portrait = m_JoePortrait;
            Regime = Regime.Carnivore;
        }

        Name = CharacterManager.Instance.Character.ToString();
    }
}

[Serializable]
public enum Regime
{
    Omnivore,
    Vegan,
    Carnivore
}