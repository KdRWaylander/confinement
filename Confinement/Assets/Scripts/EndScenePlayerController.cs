﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScenePlayerController : MonoBehaviour
{
    [SerializeField] private float m_Speed = 5f;

    private void Update()
    {
        transform.Translate(Vector3.forward * m_Speed * Time.deltaTime, Space.Self);

        if (transform.position.x >= 2f)
            gameObject.SetActive(false);
    }
}