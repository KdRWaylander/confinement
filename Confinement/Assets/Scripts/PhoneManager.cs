﻿using System.Collections;
using UnityEngine;

public class PhoneManager : MonoBehaviour
{
    [SerializeField] private GameObject m_Phone;
    [SerializeField] private IntroController m_Controller;

    private void OnEnable()
    {
        m_Controller.NewArrivedAtPosition += StartConversation;
        GameManager.Instance.NewPlayModeActivated += DiscardPhone;
    }

    private void Start()
    {
        m_Phone.SetActive(false);
    }

    private void OnDisable()
    {
        m_Controller.NewArrivedAtPosition -= StartConversation;
        GameManager.Instance.NewPlayModeActivated -= DiscardPhone;
    }

    private void StartConversation()
    {
        StartCoroutine(DisplayConversation());
    }

    private IEnumerator DisplayConversation()
    {
        yield return new WaitForSeconds(1.5f);
        
        m_Phone.SetActive(true);
        m_Phone.GetComponent<Animator>().SetTrigger("StartConversation");
    }

    private void DiscardPhone()
    {
        Destroy(GetComponentInParent<Camera>().gameObject);
    }
}