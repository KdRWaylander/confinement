﻿using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager Instance { get; private set; }

    public Character Character { get; private set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }

    public void SetCharacter (Character character)
    {
        Character = character;
    }
}

[System.Serializable]
public enum Character
{
    Josh,
    Allie,
    Joe
}