﻿using System.Collections;
using UnityEngine;

public class Fireworks : MonoBehaviour
{
    [SerializeField] private float m_WaitTime = 4.5f;
    [SerializeField] private GameObject[] m_Fireworks;

    private void Start()
    {
        StartCoroutine(LaunchFireworks());
    }

    private IEnumerator LaunchFireworks()
    {
        yield return new WaitForSeconds(m_WaitTime);

        for(int i = 0; i < Random.Range(4,7); i++)
        {
            int j = Random.Range(0, m_Fireworks.Length);
            Vector3 pos = new Vector3((float)Random.Range(-3, 1), (float)Random.Range(1, 2), 0);
            Instantiate(m_Fireworks[j], pos, Quaternion.identity);

            yield return new WaitForSeconds(.5f);
        }
    }
}