﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseCanvas : MonoBehaviour
{
    [SerializeField] private Canvas m_PauseCanvas;

    private void Start()
    {
        m_PauseCanvas.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.GameState == GameState.Play)
        {
            Time.timeScale = 0;
            m_PauseCanvas.gameObject.SetActive(true);
            GameManager.Instance.PauseGame();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.GameState == GameState.Pause)
        {
            Time.timeScale = 1;
            m_PauseCanvas.gameObject.SetActive(false);
            GameManager.Instance.UnpauseGame();
            return;
        }
    }
}