﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIEndGameCanvas : MonoBehaviour
{
    [SerializeField] private Canvas m_VictoryCanvas;
    [SerializeField] private Animator m_VictoryCanvasAnimator;
    [SerializeField] private Canvas m_DeafeatCanvas;

    private void Start()
    {
        Time.timeScale = 1;
        
        m_VictoryCanvas.gameObject.SetActive(false);
        m_DeafeatCanvas.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        GameManager.Instance.NewVictory += ToogleVictoryCanvas;
        GameManager.Instance.NewGameOver += ToogleDefeatCanvas;
    }

    private void OnDisable()
    {
        GameManager.Instance.NewVictory -= ToogleVictoryCanvas;
        GameManager.Instance.NewGameOver -= ToogleDefeatCanvas;
    }

    private void ToogleVictoryCanvas()
    {
        //Time.timeScale = 0;
        m_VictoryCanvas.gameObject.SetActive(true);
        m_VictoryCanvasAnimator.SetTrigger("Victory");

        StartCoroutine(LoadFinalScene());
    }

    private IEnumerator LoadFinalScene()
    {
        yield return new WaitForSeconds(1);

        SceneManager.LoadSceneAsync(2);
    }

    private void ToogleDefeatCanvas()
    {
        Time.timeScale = 0;
        m_DeafeatCanvas.gameObject.SetActive(true);
    }
}