﻿using System.Collections;
using UnityEngine;

public class HowToPlayCanvas : MonoBehaviour
{
    [SerializeField] private float m_TimeBeforeFade = 10f;
    [SerializeField] private Animator m_CanvasAnimator;

    private void OnEnable()
    {
        GameManager.Instance.NewPlayModeActivated += DisplayCanvas;
    }

    private void Start()
    {
        GetComponent<CanvasGroup>().alpha = 0; // Security-wise only, juste be at 0 in the inspector already
    }

    private void OnDisable()
    {
        GameManager.Instance.NewPlayModeActivated -= DisplayCanvas;
    }

    private void DisplayCanvas()
    {
        StartCoroutine(FadeCanvasInAndOut());
    }

    private IEnumerator FadeCanvasInAndOut()
    {
        m_CanvasAnimator.SetTrigger("FadeIn");

        yield return new WaitForSeconds(m_TimeBeforeFade + 1f); // 1f is fade in animation time

        m_CanvasAnimator.SetTrigger("FadeOut");

        yield return new WaitForSeconds(2f); // 2f is fade out animation time

        Destroy(gameObject);
    }
}