﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayCharacterButton : MonoBehaviour
{
    [SerializeField] private Character m_CharacterToPlay;

    public void PlayCharacter()
    {
        CharacterManager.Instance.SetCharacter(m_CharacterToPlay);
        GameManager.Instance.StartGame();
    }
}