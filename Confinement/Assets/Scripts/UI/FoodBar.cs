﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FoodBar : MonoBehaviour
{
    [SerializeField] private Image m_FillImage;
    [SerializeField] private Image m_FoodIconImage;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private Gradient m_ColorGradient;
    [SerializeField] private Sprite m_OmnivoreSprite;
    [SerializeField] private Sprite m_VeganSprite;
    [SerializeField] private Sprite m_CarnivoreSprite;

    private void Start()
    {
        UpdateFoodBarDisplay(0);

        if (Player.Instance.Regime == Regime.Omnivore)
            m_FoodIconImage.sprite = m_OmnivoreSprite;
        else if (Player.Instance.Regime == Regime.Vegan)
            m_FoodIconImage.sprite = m_VeganSprite;
        else
            m_FoodIconImage.sprite = m_CarnivoreSprite;
    }

    private void OnEnable()
    {
        Player.Instance.NewFoodPickedUp += UpdateFoodBarDisplay;
    }

    private void OnDisable()
    {
        Player.Instance.NewFoodPickedUp -= UpdateFoodBarDisplay;
    }

    private void UpdateFoodBarDisplay(int amount)
    {
        int currentFood = FoodManager.Instance.CurrentFoodAmount;
        int goalFood = FoodManager.Instance.FoodGoalToReach;
        m_FillImage.fillAmount =  (float) currentFood / goalFood;
        m_FillImage.color = m_ColorGradient.Evaluate(m_FillImage.fillAmount);
        m_AmountText.text = currentFood.ToString() + "/" + goalFood.ToString();
    }
}