﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour
{
    public static LoadManager Instance { get; private set; }

    [SerializeField] private UILoadBar m_LoadBar;
    [SerializeField] private Animator m_DoorAnimator;
    [SerializeField] private bool m_AnimateLoading;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Time.timeScale = 1; // Pause menu -> home button sets the timeScale to 0, need a script in main menu to set it back at 1
        m_LoadBar.gameObject.SetActive(false);
    }

    public void LoadLevel(int index)
    {
        StartCoroutine(LoadAsynchronously(index));
    }

    private IEnumerator LoadAsynchronously(int sceneIndex)
    {
        if(m_AnimateLoading)
        {
            m_DoorAnimator.SetTrigger("Open");
            yield return new WaitForSeconds(2f);
        }

        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        m_LoadBar.gameObject.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            m_LoadBar.DisplayProgress(progress);
            yield return null;
        }
    }
}