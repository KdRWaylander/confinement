﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UILoadButton : MonoBehaviour
{
    [SerializeField] private UILoadBar m_LoadBar;

    private void Start()
    {
        m_LoadBar.gameObject.SetActive(false);
    }

    public void LoadLevel(int index)
    {
        if (index == 0)
            GameManager.Instance.ToMainMenu();
        else if (index == 1)
            GameManager.Instance.RestartGame();

        StartCoroutine(LoadAsynchronously(index));
    }

    private IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        m_LoadBar.gameObject.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            m_LoadBar.DisplayProgress(progress);
            yield return null;
        }
    }
}