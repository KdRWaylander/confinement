﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UILoadBar : MonoBehaviour
{
    [SerializeField] private Image m_FillBar;
    [SerializeField] private TextMeshProUGUI m_FillText;

    public void DisplayProgress(float progress)
    {
        m_FillBar.fillAmount = progress;
        m_FillText.text = Mathf.FloorToInt(100 * progress) + "%";
    }
}