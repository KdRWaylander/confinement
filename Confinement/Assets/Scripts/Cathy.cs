﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cathy : MonoBehaviour, IInteractable
{
    public Sprite Portrait { get { return m_Portrait; } }

    public bool IsInteractable { get { return true; } }

    public float InteractionDistance { get { return m_InteractionDistance; } }

    [SerializeField] private Sprite m_Portrait;
    [SerializeField] private float m_InteractionDistance = 2;
    [SerializeField] private List<string> m_FoodGoalReachedLines;
    [SerializeField] private List<string> m_FoodGoalUnreachedLines;

    public void InteractWith()
    {
        if (FoodManager.Instance.FoodGoalReached)
        {
            StartCoroutine(VictorySpeech());
        }
        else
        {
            DialogManager.Instance.SayOneRandomLine(m_FoodGoalUnreachedLines, Portrait);
        }
    }

    private IEnumerator VictorySpeech()
    {
        DialogManager.Instance.SayAllLines(m_FoodGoalReachedLines, Portrait);

        yield return new WaitForSeconds(4.5f);

        GameManager.Instance.Victory();
    }
}