﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameState GameState { get; private set; }

    public event Action NewPlayModeActivated;
    public event Action NewGameOver;
    public event Action NewVictory;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        GameState = GameState.MainMenu;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && GameState == GameState.Intro)
            ActivatePlayMode();
    }

    public void StartGame()
    {
        GameState = GameState.Intro;
        LoadManager.Instance.LoadLevel(1);
    }

    public void ActivatePlayMode()
    {
        GameState = GameState.Play;
        HandleNewPlayModeActivated();
    }

    public void PauseGame()
    {
        GameState = GameState.Pause;
    }

    public void UnpauseGame()
    {
        GameState = GameState.Play;
    }

    public void Victory()
    {
        GameState = GameState.Victory;
        HandleNewVictory();
    }

    public void GameOver()
    {
        GameState = GameState.Defeat;
        HandleNewGameOver();
    }

    public void ToMainMenu()
    {
        GameState = GameState.MainMenu;
    }

    public void RestartGame()
    {
        GameState = GameState.Intro;
    }

    private void HandleNewPlayModeActivated()
    {
        if (NewPlayModeActivated != null)
            NewPlayModeActivated();
    }

    private void HandleNewVictory()
    {
        if (NewGameOver != null)
            NewVictory();
    }

    private void HandleNewGameOver()
    {
        if (NewGameOver != null)
            NewGameOver();
    }
}

[System.Serializable]
public enum GameState
{
    MainMenu,
    Intro,
    Play,
    Pause,
    Defeat,
    Victory
}