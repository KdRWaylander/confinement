﻿using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private bool m_OverrideGameState;
    
    private NavMeshAgent m_NavMeshAgent;
    private Animator m_Animator;

    private void Awake()
    {
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        m_Animator = transform.GetChild(0).GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if (m_OverrideGameState == true)
            return;

        GameManager.Instance.NewVictory += StopWalking;
        GameManager.Instance.NewGameOver += StopWalking;
    }

    private void Update()
    {
        if (m_OverrideGameState == false && GameManager.Instance.GameState != GameState.Play)
            return;

        if (Input.GetMouseButtonUp(0)) // Left click
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit) && hit.transform.GetComponent<IInteractable>() != null && hit.transform.GetComponent<IInteractable>().IsInteractable)
            {
                if(Vector3.Distance(hit.transform.position, transform.position) > hit.transform.GetComponent<IInteractable>().InteractionDistance)
                    m_NavMeshAgent.SetDestination(hit.transform.position);
                else
                {
                    hit.transform.GetComponent<IInteractable>().InteractWith();
                }
            }
        }

        if (Input.GetMouseButtonUp(1)) // Right click
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                m_NavMeshAgent.SetDestination(hit.point);
            }
        }

        if(Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl))
        {
            m_NavMeshAgent.ResetPath();
        }

        m_Animator.SetFloat("Velocity", m_NavMeshAgent.velocity.magnitude);
    }

    private void OnDisable()
    {
        if (m_OverrideGameState == true)
            return;

        GameManager.Instance.NewVictory -= StopWalking;
        GameManager.Instance.NewGameOver -= StopWalking;
    }

    private void StopWalking()
    {
        m_NavMeshAgent.ResetPath();
    }
}