﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CharacterPreview : MonoBehaviour
{
    [SerializeField] private GameObject m_Character;
    [SerializeField] private GameObject m_RegimeHolder;
    [SerializeField] private GameObject m_Allie;
    [SerializeField] private TextMeshProUGUI m_AllieRegime;
    [SerializeField] private GameObject m_Josh;
    [SerializeField] private TextMeshProUGUI m_JoshRegime;
    [SerializeField] private GameObject m_Joe;
    [SerializeField] private TextMeshProUGUI m_JoeRegime;

    private void Start()
    {
        m_Character.SetActive(false);
    }

    public void ShowAllie()
    {
        m_Character.SetActive(true);
        m_Allie.SetActive(true);
        m_Josh.SetActive(false);
        m_Joe.SetActive(false);

        m_RegimeHolder.SetActive(true);
        m_AllieRegime.gameObject.SetActive(true);
        m_JoshRegime.gameObject.SetActive(false);
        m_JoeRegime.gameObject.SetActive(false);
    }

    public void ShowJosh()
    {
        m_Character.SetActive(true);
        m_Allie.SetActive(false);
        m_Josh.SetActive(true);
        m_Joe.SetActive(false);

        m_RegimeHolder.SetActive(true);
        m_AllieRegime.gameObject.SetActive(false);
        m_JoshRegime.gameObject.SetActive(true);
        m_JoeRegime.gameObject.SetActive(false);
    }

    public void ShowJoe()
    {
        m_Character.SetActive(true);
        m_Allie.SetActive(false);
        m_Josh.SetActive(false);
        m_Joe.SetActive(true);

        m_RegimeHolder.SetActive(true);
        m_AllieRegime.gameObject.SetActive(false);
        m_JoshRegime.gameObject.SetActive(false);
        m_JoeRegime.gameObject.SetActive(true);
    }

    public void HideAll()
    {
        m_Character.SetActive(false);
        m_RegimeHolder.SetActive(false);
    }
}