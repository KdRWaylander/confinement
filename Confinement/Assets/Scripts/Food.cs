﻿using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour, ICollectible, IInteractable
{
    public int Amount { get { return m_Amount; } }

    public GameObject PickUpParticle { get { return m_ParticleEffect; } }

    public bool IsCollected { get { return m_IsCollected; } }

    public bool IsInteractable { get { return !m_IsCollected; } }

    public float InteractionDistance { get { return m_InteractionDistance; } }

    public List<string> CanEatLines { get { return m_CanEatLines; } }
    public List<string> CannotEatLines { get { return m_CannotEatLines; } }

    [SerializeField] private int m_Amount;
    [SerializeField] private GameObject m_ParticleEffect;
    [SerializeField] private float m_InteractionDistance = 1;
    [SerializeField] private List<string> m_CanEatLines;
    [SerializeField] private List<string> m_CannotEatLines;

    private bool m_IsCollected;

    private void Start()
    {
        m_IsCollected = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null && IsCollected == false)
        {
            PickUpCollectible(other.GetComponent<Player>());
        }
    }

    public void PickUpCollectible(Player player)
    {
        m_IsCollected = true;
        GetComponent<MeshRenderer>().enabled = false;

        bool isCompliant = CheckFoodComplianceWith(player);
        if (isCompliant)
            Instantiate(PickUpParticle, transform);
        player.AddFood(this, isCompliant);

        Destroy(gameObject, 1);
    }

    public void InteractWith()
    {
        PickUpCollectible(Player.Instance);
    }

    private bool CheckFoodComplianceWith(Player player)
    {
        if(transform.tag == "FruitsVegetables" && player.Regime == Regime.Carnivore || transform.tag == "MeatCheeseWine" && player.Regime == Regime.Vegan)
            return false;
        else
            return true;
    }
}