﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollectible
{
    GameObject PickUpParticle { get; }
    bool IsCollected { get; }
    void PickUpCollectible(Player player);
}