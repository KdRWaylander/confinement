﻿public interface IInteractable
{
    bool IsInteractable { get; }
    float InteractionDistance { get; }

    void InteractWith();
}