﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }

    [SerializeField] private AudioClip m_AudioClip;
    private AudioSource m_AudioSource;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);

        m_AudioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        m_AudioSource.Play();
        m_AudioSource.loop = true;
    }
}