﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadMenu : MonoBehaviour {
    [SerializeField] Image m_ProgressImage;

    private void Start()
    {
        m_ProgressImage.fillAmount = 0;
    }

    public void LoadScene(int _index)
    {
        StartCoroutine(LoadAsynchronously(_index));
    }

    IEnumerator LoadAsynchronously(int _i)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(_i);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            m_ProgressImage.fillAmount = progress;
            yield return null;
        }
    }
}