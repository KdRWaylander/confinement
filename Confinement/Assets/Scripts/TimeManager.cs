﻿using UnityEngine;
using TMPro;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance { get; private set; }
    
    [SerializeField] private int m_SecondsBeforeClosing = 300;
    [SerializeField] private Canvas m_TimerCanvas;
    [SerializeField] private TextMeshProUGUI m_TimerText;

    private int m_RemainingSeconds;
    private bool m_IsGameOver;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        m_TimerCanvas.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        GameManager.Instance.NewPlayModeActivated += StartClock;
        
        GameManager.Instance.NewVictory += StopClock;

        GameManager.Instance.NewGameOver += StopClock;
        GameManager.Instance.NewGameOver += HideCanvas;
    }

    private void Update()
    {
        if(GameManager.Instance.GameState == GameState.Play && m_RemainingSeconds <= 0 && m_IsGameOver == false)
        {
            m_IsGameOver = true;
            GameManager.Instance.GameOver();
        }
    }

    private void OnDisable()
    {
        GameManager.Instance.NewPlayModeActivated -= StartClock;

        GameManager.Instance.NewVictory -= StopClock;

        GameManager.Instance.NewGameOver -= StopClock;
        GameManager.Instance.NewGameOver -= HideCanvas;
    }

    private void DecreaseTime()
    {
        m_RemainingSeconds -= 1;
        DisplayTime(FormatTimeToString(m_RemainingSeconds));
    }

    private string FormatTimeToString(int time)
    {
        string mins = (time / 60).ToString();
        mins = mins.Length < 2 ? "0" + mins : mins;

        string secs = (time % 60).ToString();
        secs = secs.Length < 2 ? "0" + secs : secs;

        return mins + ":" + secs;
    }

    private void DisplayTime(string time)
    {
        m_TimerText.text = time;
    }

    private void StartClock()
    {
        m_RemainingSeconds = m_SecondsBeforeClosing;
        m_IsGameOver = false;
        InvokeRepeating("DecreaseTime", 0, 1);

        m_TimerCanvas.gameObject.SetActive(true);
    }

    private void StopClock()
    {
        CancelInvoke("DecreaseTime");
    }

    private void HideCanvas()
    {
        m_TimerCanvas.gameObject.SetActive(false);
    }
}